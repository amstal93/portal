const mix = require('laravel-mix');

const VuetifyLoaderPlugin = require('vuetify-loader/lib/plugin');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .setPublicPath('./public')
    .js('resources/js/app.js', 'js')
    .sass('resources/sass/app.scss', 'public/css/app.css')
    //.styles('resources/css/*.css', 'public/css/app.css')
    .copy('resources/img/*', 'public/img')
    .webpackConfig({
        plugins: [
            new VuetifyLoaderPlugin({
                options: {}
            }),
        ]
    })
    .version()
    .sourceMaps(false)
    .vue({
        extractVueStyles: true, // Extract .vue component styling to file, rather than inline.
    });
