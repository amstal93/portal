<?php

declare(strict_types=1);

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SocialmediaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        // TODO add the icon labels when we decide on an icon library that has all these
        DB::table('socialmedia')->insert([
            'name' => 'Facebook',
            'url' => 'https://facebook.com/spaceleiden',
            'icon' => 'mdi-facebook',
        ]);
        DB::table('socialmedia')->insert([
            'name' => 'Instagram',
            'url' => 'https://instagram.com/spaceleiden',
            'icon' => 'mdi-instagram',
        ]);
        DB::table('socialmedia')->insert([
            'name' => 'Twitter',
            'url' => 'https://twitter.com/spaceleiden',
            'icon' => 'mdi-twitter',
        ]);
        DB::table('socialmedia')->insert([
            'name' => 'Discord',
            'url' => 'https://discord.gg/X6qffAn',
            'icon' => 'mdi-discord',
        ]);
        DB::table('socialmedia')->insert([
            'name' => 'WhatsApp',
            'url' => 'https://chat.whatsapp.com/FNV2e0CrJdqFYZzqblx8CW',
            'icon' => 'mdi-whatsapp',
        ]);
        DB::table('socialmedia')->insert([
            'name' => 'LinkedIn',
            'url' => 'https://linkedin.com/company/spaceleiden',
            'icon' => 'mdi-linkedin',
        ]);
        DB::table('socialmedia')->insert([
            'name' => 'Gitlab',
            'url' => 'https://gitlab.com/spaceleiden',
            'icon' => 'mdi-gitlab',
        ]);
    }
}
