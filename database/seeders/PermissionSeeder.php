<?php

declare(strict_types=1);

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionSeeder extends Seeder
{
    public function run()
    {
        $admin_role = Role::updateOrCreate([
            'name' => 'admin',
        ]);

        Role::updateOrCreate([
            'name' => 'member',
        ]);

        Permission::updateOrCreate([
            'name' => 'add_activities',
            'guard_name' => 'web',
        ]);
        Permission::updateOrCreate([
            'name' => 'edit_activities',
            'guard_name' => 'web',
        ]);
        Permission::updateOrCreate([
            'name' => 'add_news',
            'guard_name' => 'web',
        ]);

        $admin_role->givePermissionTo(['add_activities', 'edit_activities', 'add_news']);
        // TODO: $subscriber_role->giverPermissionTo(['...']);
        //       make sure only a member/admin can view home attributes, videos etc.
    }
}
