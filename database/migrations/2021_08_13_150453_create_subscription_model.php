<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubscriptionModel extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('subscriptions', function (Blueprint $table) {
            $table->string('description');
            $table->string('amount');
            $table->string('currency')->default('EUR');
            $table->string('start_date')->default('first day of next month');
            $table->string('interval')->default('1 month');
            $table->string('mandate_title');
            $table->boolean('default')->default(false);
            $table->timestamps();
        });

        DB::table('subscriptions')->insert([
            'description' => 'The Space Leiden Deelnemerschap',
            'mandate_title' => 'The Space Leiden Deelnemerschap Digitaal Mandaat',
            'amount' => '5.00',
            'default' => true,
        ]);
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::dropIfExists('subscriptions');
    }
}
