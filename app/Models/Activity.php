<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'activity_start',
        'title',
        'description',
        'type',
        'location',
        'speaker',
        'max_participants',
        'shopping_list',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'current_user_registered',
        'activity_start_date_info',
    ];

    /**
     * Get the registrations for the activity.
     */
    public function registrations()
    {
        return $this->hasMany(Registration::class);
    }

    /**
     * Get the attending users for the activity.
     */
    public function attendees()
    {
        return $this->belongstoMany(User::class, 'registrations')->whereNull('registrations.deleted_at');
    }

    /**
     * @return bool
     */
    public function getCurrentUserRegisteredAttribute()
    {
        $user = request()->user();
        if ($user) {
            return $this->registrations->where('user_id', $user->id)->count() > 0;
        }
        return false;
    }

    /**
     * Get the split up info from the start datetime.
     *
     * @return array
     */
    public function getActivityStartDateInfoAttribute()
    {
        $activity_start_date_info = [
            'datetime' => $this->activity_start,
            'date' => date('Y-m-d', strtotime($this->activity_start)),
            'time' => date('H:i', strtotime($this->activity_start)),
        ];
        return $activity_start_date_info;
    }
}
