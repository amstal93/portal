<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Models\Activity;
use Illuminate\Http\Request;

class ActivityController extends Controller
{
    /**
     * Get all past and future activities, split on start datetime
     *
     * might want to add a $count parameter to be able to get the last and next $count activities
     */
    public function index(Request $request)
    {
        $past_activities = Activity::where('activity_start', '<', now())->orderBy('activity_start', 'desc')->get();
        $future_activities = Activity::where('activity_start', '>=', now())->orderBy('activity_start', 'asc')->get();
        // would it be faster to split this
        // - in the query or
        // - to collect all first and then split in php or
        // - to pass this to frontend and split in js?
        return response()->success(__('responses.success'), [
            'past_activities' => $past_activities->toArray(),
            'future_activities' => $future_activities->toArray(),
        ]);
    }
}
