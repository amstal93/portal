<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreRegistrationRequest;
use App\Models\Registration;
use App\Models\User;
use Illuminate\Http\Request;

class RegistrationController extends Controller
{
    public function getForUser(Request $request, $user_id)
    {
        $registrations = Registration::where('user_id', $user_id)->with('activity')->get();
        return response()->success(__('responses.success'), [
            'registrations' => $registrations->toArray(),
        ]);
    }

    public function store(StoreRegistrationRequest $request)
    {
        $validated = $request->validated();

        if (Registration::where('activity_id', $validated['activity_id'])->where('user_id', $validated['user_id'])->where('deleted_at', null)->exists()) {
            // should not happen unless user gets registered while admin has already loaded the modal
            return response()->statusError(409, __('responses.admin.registraties.double'));
        }

        // TODO implement different registration purposes to admins register user feature
        $validated['purpose'] = 'workshop';

        $registration = Registration::create($validated);
        $user = User::find($validated['user_id']);
        return response()->success(__('responses.success'), [
            'registration' => $registration,
            'attendee' => $user,
        ]);
    }

    public function remove(Request $request, Registration $registration)
    {
        $registration->delete();
        return response()->success(__('responses.success'));
    }
}
