@component('mail::message')
Welkom bij de Space Leiden, **{{ $name }}**!

Bedankt voor je aanmelding als deelnemer. Met jouw maandelijkse bijdrage kunnen
we onze visie van een hackerspace en makerspace in Leiden voortzetten.

Als deelnemer krijg je meteen toegang tot het deelnemersportaal. Op deze website
kan je apparaten en gereedschap reserveren, je persoonlijke gegevens inzien en heb
je toegang tot het archief van opgenomen workshops en talks.

@component('mail::button', ['url' => 'https://portal.spaceleiden.nl/login'])
Inloggen op Portaal
@endcomponent

@component('mail::panel')
De eerstvolgende afschrijving van de deelnemersbijdrage van
**{{ $subscription_cost->value }} {{ $subscription_cost->currency }}** zal
plaatsvinden op of rond **{{ $next_subscription_payment }}**.
@endcomponent

Mocht je nog vragen hebben, stuur ons dan een [mailtje](mailto:bestuur@spaceleiden.nl)
of spreek iemand met een rood vest aan op de space.

Tot volgende donderdag,<br>
Team The Space Leiden
@endcomponent
