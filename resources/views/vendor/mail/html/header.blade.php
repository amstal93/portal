<tr>
<td class="header">
<a href="{{ $url }}" style="display: inline-block;">
@if (trim($slot) === 'Stichting The Space Leiden')
<img src="https://spaceleiden.nl/assets/img/logo.png" class="logo" alt="The Space Logo">
@else
{{ $slot }}
@endif
</a>
</td>
</tr>
