import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import '@mdi/font/css/materialdesignicons.css'

Vue.use(Vuetify)

const opts = {
  theme: {
    options: { customProperties: true },
    themes: {
      light: {
        primary: '#003469',
        secondary: '#EFC10E',
        accent: '#E88D04'
      },
      dark: {
        primary: '#EFC10E',
        secondary: '#003469',
        accent: '#E88D04'
      }
    }
  },
  icons: {
    iconfont: 'mdi'
  }
}

if (localStorage.getItem('dark_theme') === 'true') {
  opts.theme.dark = true
}

export default new Vuetify(opts)
