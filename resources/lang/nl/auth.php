<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'De logincombinatie is niet juist.',
    'password' => 'Het wachtwoord is niet juist.',
    'throttle' => 'Te veel loginpogingen. Probeer opnieuw over :seconds seconden.',
    'unauthenticated' => 'Authenticatie is vereist voor deze aanvraag.',
    'success' => 'Login is gelukt!',
];
