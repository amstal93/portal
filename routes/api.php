<?php

declare(strict_types=1);

use App\Http\Controllers\ActivityController;
use App\Http\Controllers\Admin\ActivityController as AdminActivityController;
use App\Http\Controllers\Admin\ArchiveController as AdminArchiveController;
use App\Http\Controllers\Admin\NewsController as AdminNewsController;
use App\Http\Controllers\Admin\RegistrationController as AdminRegistrationController;
use App\Http\Controllers\Admin\SettingsController as AdminSettingsController;
use App\Http\Controllers\Admin\UsersController as AdminUsersController;
use App\Http\Controllers\ArchiveController;
use App\Http\Controllers\Auth\AuthController;
use App\Http\Controllers\DonationController;
use App\Http\Controllers\FinanceController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\RegistrationController;
use App\Http\Controllers\SocialmediaController;
use App\Http\Controllers\StatusController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * Public API calls.
 */
Route::group(['prefix' => '/public', 'middleware' => 'cors'], function () {
    Route::get('/status', [StatusController::class, 'index']);
    Route::get('/status.json', [StatusController::class, 'index']);
});

/**
 * Authentication related API calls.
 *
 */
Route::group(['prefix' => '/auth'], function() {

    // Login area auth actions.
    Route::post('/signup', [UserController::class, 'signup'])->name('signup');
    Route::post('/login', [AuthController::class, 'login'])->name('login');
    Route::post('/forgot', [AuthController::class, 'forgot'])->name('forgot');
    Route::post('/reset', [AuthController::class, 'reset'])->name('reset');
});

/**
 * Payment related API calls.
 *
 */
Route::group(['prefix' => '/payments'], function() {

    // Mollie callbacks and webhooks.
    Route::get('/callback', [FinanceController::class, 'callback'])->name('callback');
    Route::post('/webhook', [FinanceController::class, 'webhook'])->name('webhook');
    Route::post('/donate/webhook', [DonationController::class, 'webhookDonationPayment']);

    // Call on signup page to display correct subscription amount.
    Route::get('/subscription/default', [FinanceController::class, 'getDefaultSubscription']);

    // User actions on their payments.
    Route::get('/subscription/cancel', [FinanceController::class, 'cancel'])->middleware('auth:sanctum')->name('cancel');
    Route::get('/subscription/reactivate', [FinanceController::class, 'reactivate'])->middleware('auth:sanctum')->name('reactivate');
    Route::get('/mandate/create', [UserController::class, 'createNewMandatePayment'])->middleware('auth:sanctum')->name('createNewMandatePayment');

    // Member donations
    Route::group(['middleware' => 'auth:sanctum', 'role:member'], function() {
        Route::get('/donate/create/{amount}', [DonationController::class, 'createOneTimeMemberDonation']);
        Route::post('/donate/recurring/create', [DonationController::class, 'createRecurringMemberDonation']);
        Route::post('/donate/recurring/cancel', [DonationController::class, 'cancelRecurringMemberDonation']);
        Route::get('/donate/callback', [DonationController::class, 'callbackDonationPayment']);
    });

    // Anonymous donations
    // Route::get('/donation/create/{amount}', [DonationController::class, 'createOneTimeAnonymousDonation']);

});

/**
 * User area API calls.
 *
 */
Route::group(['prefix' => '/me', 'middleware' => 'auth:sanctum'], function() {
  Route::get('/finance', [UserController::class, 'finance'])->name('finance');
  Route::get('/payments', [UserController::class, 'payments'])->name('payments');
  Route::get('/', [UserController::class, 'me'])->name('me');
  Route::patch('/', [UserController::class, 'update'])->name('update');
});

/**
 * Member area API calls.
 *
 */
Route::group(['middleware' => 'auth:sanctum', 'role:member'], function(){
    Route::get('/activities', [ActivityController::class, 'index']);
    Route::post('/registrations', [RegistrationController::class, 'store']);
    Route::delete('/registrations', [RegistrationController::class, 'destroy']);
    Route::get('/news/internal', [NewsController::class, 'index']);
    Route::get('/news/external', [NewsController::class, 'blogposts']);
    Route::get('/socialmedia', [SocialmediaController::class, 'index']);

    Route::group(['prefix' => '/archive'], function(){
        Route::get('/list/{path?}', [ArchiveController::class, 'list'])->where('path', '(.*)');
        Route::get('/download/{path}', [ArchiveController::class, 'get'])->where('path', '(.*)');
    });

});

/**
 * Admin area API calls.
 *
 */
Route::group(['prefix' => '/admin', 'middleware' => ['auth:sanctum', 'role:admin']], function() {

    // Activities
    Route::get('/activities', [AdminActivityController::class, 'index']);
    Route::post('/activities', [AdminActivityController::class, 'store']);
    Route::patch('/activities/{activity}', [AdminActivityController::class, 'update']);
    Route::delete('/activities/{activity}', [AdminActivityController::class, 'remove']);

    // Registrations
    Route::get('/registrations', [AdminActivityController::class, 'registrationCount']);
    Route::post('/registrations', [AdminRegistrationController::class, 'store']);
    Route::delete('/registrations/{registration}', [AdminRegistrationController::class, 'remove']);

    // News
    Route::post('/news/internal', [AdminNewsController::class, 'store']);
    Route::patch('/news/internal/{news}', [AdminNewsController::class, 'store']);
    Route::delete('/news/internal/{news}', [AdminNewsController::class, 'remove']);

    // Users
    Route::get('/users', [AdminUsersController::class, 'index']);
    Route::get('/users/birthdays', [AdminUsersController::class, 'birthdays']);
    Route::get('/users/{user_id}/finance', [AdminUsersController::class, 'finance']);
    Route::get('/users/{user_id}/finance/payments', [AdminUsersController::class, 'payments']);
    Route::get('/users/{user_id}/registrations', [AdminRegistrationController::class, 'getForUser']);

    // Settings
    Route::get('/settings', [AdminSettingsController::class, 'index']);
    Route::patch('/settings', [AdminSettingsController::class, 'update']);

    // Archive
    Route::group(['prefix' => '/archive'], function () {
        Route::post('/file/{path?}', [AdminArchiveController::class, 'createFile'])->where('path', '(.*)');
        Route::delete('/file/{path}', [AdminArchiveController::class, 'deleteFile'])->where('path', '(.*)');
        Route::post('/folder/{path}', [AdminArchiveController::class, 'createFolder'])->where('path', '(.*)');
        Route::delete('/folder/{path}', [AdminArchiveController::class, 'deleteFolder'])->where('path', '(.*)');
    });
});
