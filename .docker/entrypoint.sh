#!/usr/bin/env bash
set -e

role=${CONTAINER_ROLE:-app}

if [ ! -f ".env" ]; then
    cp .env.example .env
fi

source .env

if [ -z "$APP_KEY" ]; then
    echo "No application key found, setting and then exiting. Please start again.";
    php artisan key:generate
    source .env
fi

if [ "$role" = "app" ]; then
    php artisan config:cache
    php artisan event:cache
    php artisan route:cache
    php artisan view:cache

    sleep 5

    if [ "$APP_ENV" == "staging" ] || [ "$APP_ENV" == "local" ]; then
        php artisan migrate:fresh --seed
    else
        php artisan migrate --force
    fi

    chown www-data:www-data storage -R
    exec docker-php-entrypoint "$@"
elif [ "$role" = "queue" ]; then
    echo "Running the queue..."
    php artisan queue:work --verbose --memory=256M --tries=1 --timeout=120 --no-interaction
elif [ "$role" = "scheduler" ]; then
    while true
    do
      php -d memory_limit=1G artisan schedule:run --verbose --no-interaction &
      sleep 60
    done
elif [ "$role" = "websocket" ]; then
    php artisan websocket:serve
else
    echo "Could not match the container role \"$role\""
    exit 1
fi
